import json
import socket
import sys, os, traceback
import random
import math

def arrayPrettyPrint( name, aList ):
    return name + ":\n" + "\t".join("%6.2f"%x for x in aList)

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.angle = 0.0
        self.pieceIndex = 0
        self.turboAvailable = False
        self.crashedInThisLap = False
        self.lapTimes = []
        self.lap = 0
        self.logLevel = 2 if "HWO_kskowron_log_level" not in os.environ else int(os.environ["HWO_kskowron_log_level"])
        self.qual = True

    def printL(self, level, *args ):
        try:
                try:
                    if self.logLevel >= level:
                        lap = "lap: " + str(self.car_position["piecePosition"]["lap"])
                        track = self.track["id"]
                        print( " ".join(map(str, [track,lap] + list(args) )))
                except:
                    if level >= self.logLevel:
                        print( args )
        except:
                self.ping()

    def msg(self, msg_type, data):
        log.write( "\t".join(["OUT",msg_type,str(data)]) + "\n")
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})
    
    def joinRace(self, track, carCount=4, create=False ):
        botId = {"name": self.name,"key": self.key}
        data = {"botId":botId,"trackName": track, "carCount":carCount }
        self.printL(2,"joinRace", track, carCount, create)
        return self.msg("createRace" if create else "joinRace", data)

    def throttle(self, throttle):
        self.msg("throttle", throttle)
        self.currentThrottle = throttle

    def switchLane(self, direction):
        self.printL(2,"Switch Lane " + str(direction))
        self.msg("switchLane", direction)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        if "HWO_kskowron_create" in os.environ:
            bot.joinRace( os.environ["HWO_kskowron_create"], create=True )
        elif "HWO_kskowron_join" in os.environ:
            bot.joinRace( os.environ["HWO_kskowron_join"] )
        else:
            bot.join()
        self.msg_loop()

    def on_turboAvailable(self, data):
        self.turboAvailable = True
    
    def on_gameInit(self, data):
        self.ping()
        self.qual = False if "HWO_kskowron_race" in os.environ else self.qual

        if 'track' not in dir(self) or self.track["id"] != data["race"]["track"]["id"]:
            self.printL(2,"Initializing track...")
            self.track = data["race"]["track"]
            self.max = [0.5]*len(self.track["pieces"])
            self.history = [0.]*len(self.track["pieces"])
        else:
            self.printL(2,"Skipping track initialization")

    def turbo(self):
        if self.turboAvailable:
            self.printL(2,"Turbo")
            self.msg("turbo","Turbo on")
            self.turboAvailable = False
        else:
            self.printL(2,"No turbo...")

    def on_join(self, data):
        self.printL(2,"Joined")
        self.ping()

    def on_your_car(self, data):
        self.my_car = data["color"]


    def on_game_start(self, data):
        self.printL(1,"Race started","In qualifications:",self.qual)
        self.ping()

    def on_lapFinished(self, data):
        self.ping()
        if data["car"]["color"] != self.my_car: return
        
        nxt = lambda i: i+1 if i+1 < len(self.track["pieces"]) else 0

        self.printL(2,arrayPrettyPrint( "history", self.history ))

        for i,val in enumerate(self.max):
            cap = val if self.history[i] > 30. or self.history[nxt(i)] > 15. else 1.
            weight = 0.01 + (0.05 if not self.qual else 0.2)*(1.-val)
            self.max[i] = max(0.1, min( cap, val + weight*(1.-self.history[i]/45.)*(1.-self.history[nxt(i)]/30.) ))
        
        self.history = map( lambda x: x*0.95, self.history )

        self.lapTimes.append("%.2f"%(data["lapTime"]["millis"]/1000.) + ("s (!)" if self.crashedInThisLap else "s"))

        self.printL(2,arrayPrettyPrint( "new max", self.max ))
        self.printL(1,"lap finished " + self.lapTimes[-1])
        self.crashedInThisLap = False


    def on_car_positions(self, data):
        myData = next( x for x in data if x["id"]["color"] == self.my_car ) 
        self.car_position = myData
        self.pieceIndex = myData["piecePosition"]["pieceIndex"]
        old = self.angle
        angle = abs(myData["angle"])
        self.history[self.pieceIndex] = max( angle, self.history[self.pieceIndex] )
        log.write("angle\t" + str(angle) + "\n")
        dAngle = old - angle
        sgn = 0 if dAngle == 0 else dAngle/abs(dAngle)
        throttle1 = sgn*math.sqrt(abs(dAngle)/5.)*5
        norm = lambda x: max(0.0,min(self.max[self.pieceIndex],x))
        throttle2 = abs(angle)/60.
        self.throttle( norm(1. - throttle1 - throttle2) )
        self.angle = angle
        
        if random.random() < 0.01 and self.currentThrottle >= 0.9:
            self.turbo()
        if random.random() < 0.005:
            self.switchLane( random.choice(["Left", "Right"]) )

        
    def on_crash(self, data):
        self.ping()
        if data["color"] != self.my_car:
            return
        
        prev = lambda i: i-1 if i != 0 else len(self.track["pieces"])-1
        
        self.crashedInThisLap = True
        log = []
        for piece in [self.pieceIndex, prev(self.pieceIndex), prev(prev(self.pieceIndex))]:
            self.max[piece] = max(0.1, self.max[piece]-0.1)
            log.append( self.max[piece] )
        self.printL(1, "crashed", "piece", str(piece), "\t".join( "%.2f"%(x,) for x in reversed(log) ) )
        
        self.max = map( lambda x: x*0.9, self.max )

    def on_game_end(self, data):
        self.printL(1, "Race ended\n","\t".join(self.lapTimes))
        self.crashedInThisLap = False
        self.qual = not self.qual
        self.ping()

    def on_error(self, data):
        self.printL(0,"Error: {0}".format(data))
        self.ping()

    def on_tournamentEnd(self, data):
        self.qual = True
        self.ping()


    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.on_your_car,
            'turboAvailable': self.on_turboAvailable,
            'gameInit': self.on_gameInit,
            'lapFinished': self.on_lapFinished,
            'tournamentEnd': self.on_tournamentEnd,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
          try:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            log.write( "\t".join(["IN",msg_type,str(msg)]) + "\n" )
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                self.printL(2,"Got {0}".format(msg_type))
                self.ping()
          except:
            self.printL(0, "Catastrophic failure: " + traceback.format_exc() )
            self.ping()
          line = socket_file.readline()
          


log = None
if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        class dummy(object):
            def write(self,*args,**kargs):
                pass
        log = dummy() #file("NoobBot.log","w")
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
